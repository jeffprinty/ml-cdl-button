
const path = require('path');

const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const validate = require('webpack-validator');
const combineLoaders = require('webpack-combine-loaders');

const webpackConfig = {
  context: path.resolve(__dirname, 'examples'),

  devServer: {
    https: false,
    host: '0.0.0.0',
    port: '8000',

    contentBase: path.resolve(__dirname, 'src'),
    historyApiFallback: true,
    compress: true,

    hot: true,
    inline: true,

    // --progress - [assets, children, chunks, colors, errors, hash, timings, version, warnings]
    stats: {
      assets: true,
      children: true,
      chunks: false,
      colors: true,
      errors: true,
      errorDetails: true, //depends on {errors: true}
      hash: true,
      modules: false,
      publicPath: true,
      reasons: false,
      source: true, //what does this do?
      timings: true,
      version: true,
      warnings: true
    }
  },

  // devtool: 'cheap-module-eval-source-map', //javascript sourcemaps
  devtool: 'eval', //javascript sourcemaps

  entry: {
    app: [
      'react-hot-loader/patch',
      '../examples/index.js'
    ]
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: 'http://localhost:8000/', // Use absolute paths to avoid the way that URLs are resolved by Chrome when they're parsed from a dynamically loaded CSS blob. Note: Only necessary in Dev.
    filename: '[name].bundle.js',
    library: 'Library',
    libraryTarget: 'umd',
    umdNamedDefine: true
  },

  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loader: 'babel',
        include: [
          path.resolve(__dirname, 'examples'),
          path.resolve(__dirname, 'src')
        ],
        exclude: [
          path.resolve(__dirname, 'src/assets/vendors')
        ],
      },
      {
        test: /\.css$/,
        include: [
          path.resolve(__dirname, 'src/assets/styles'),
          path.resolve(__dirname, 'src/components'),
          path.resolve(__dirname, 'examples')
        ],
        exclude: [
          path.resolve(__dirname, 'src/assets/styles/globals')
        ],
        loader: combineLoaders([
          {
            loader: 'style'
          },
          {
            loader: 'css',
            query: {
              modules: true,
              localIdentName: '[folder]__[local]--[hash:base64:10]',
              sourceMap: true,
              import: false,
              url: true
            }
          },
          {
            loader: 'resolve-url',
            query: {
              sourceMap: true,
              silent: false,
              fail: true,
              keepQuery: true
            }
          },
          {
            loader: 'postcss'
          }
        ])
      },
      {
        test: /\.css$/,
        include: [
          path.resolve(__dirname, 'src/assets/styles/globals')
        ],
        loader: combineLoaders([
          {
            loader: 'style'
          },
          {
            loader: 'css',
            query: {
              modules: true,
              localIdentName: '[local]',
              sourceMap: true,
              import: false,
              url: true
            }
          },
          {
            loader: 'resolve-url',
            query: {
              sourceMap: true,
              silent: false,
              fail: true,
              keepQuery: true
            }
          },
          {
            loader: 'postcss'
          }
        ])
      },
      // Font Definitions
      {
        test: /\.ttf$/,
        include: [
          path.resolve(__dirname, 'src/assets/fonts')
        ],
        loader: 'url',
        query: {
          limit: 10000,
          mimetype: 'application/octet-stream',
          name: 'assets/fonts/[name].[ext]'
        }
      },
      // Image Definitions
      {
        test: /\.(svg|png)$/,
        include: [
          path.resolve(__dirname, 'src/assets/icons'),
          path.resolve(__dirname, 'src/assets/images')
        ],
        loader: 'file',
        query: {
          limit: 10000,
          name: 'assets/icons/[name].[ext]'
        }
      },
      {
        test: /\.ico$/,
        include: [
          path.resolve(__dirname, 'src/assets')
        ],
        loader: 'file',
        query: {
          limit: 10000,
          name: 'assets/[name].[ext]'
        }
      }
    ]
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: 'index.html',
      inject: true,
      hash: true,
      cache: true,
      // chunks: ['app'],
      chunksSortMode: 'dependency',
      showErrors: true
    }),
    // Enable multi-pass compilation for enhanced performance
    // in larger projects. Good default.
    new webpack.HotModuleReplacementPlugin({
      multiStep: true
    }),
    //see possible syntax errors at the browser console instead of hmre overlay
    new webpack.NoErrorsPlugin()
  ],

  postcss: (_webpack) => {
    return [
      require('postcss-smart-import')({
        addDependencyTo: _webpack,
        path: [ 'styles', 'components'],
        root: path.resolve(__dirname, 'src'),
        skipDuplicates: true
      }),
      require('postcss-cssnext')()
    ];
  },

  resolve: {
    extensions: ['', '.js', '.jsx']
  }
};


module.exports = validate(webpackConfig, {
  rules: {
    'no-root-files-node-modules-nameclash': true, //default
    'loader-enforce-include-or-exclude': false,
    'loader-prefer-include': false
  }
});
