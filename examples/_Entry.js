import React, { Component } from 'react';

import MLButton from '../src/components/MLButton/MLButton';

import MLIcon from 'ml-react-cdl-icons';
import css from './_entry.css';


class _Entry extends Component {
  constructor() {
    super();
  }

 
  render() {
    return (
      <div className={ css['container']}>
        <h1>ML CDL Button Component</h1>
        <div className={css['demoWrap']}>
          <h2>Buttons</h2>
          <h3>Primary</h3>
          <MLButton title='Primary ' btnClass='primary' />
          <MLButton title='Primary Red' btnClass='primary' secondary='red' />
          <MLButton title='Primary Green' btnClass='primary' secondary='green' />
           <pre>{`
<MLButton title='Primary ' btnClass='primary' />
<MLButton title='Primary Red' btnClass='primary' secondary='red' />
<MLButton title='Primary Green' btnClass='primary' secondary='green' />
            `}</pre>
          <h3>Secondary</h3>
          <MLButton title='Secondary' btnClass='secondary' />
          <MLButton title='Secondary Red' btnClass='secondary' secondary='red' />
          <MLButton title='Secondary Green' btnClass='secondary' secondary='green' />
          <pre>{`
<MLButton title='Secondary' btnClass='secondary' />
<MLButton title='Secondary Red' btnClass='secondary' secondary='red' />
<MLButton title='Secondary Green' btnClass='secondary' secondary='green' />
          `}</pre>
          <h3>With icon</h3>
          <MLButton icon={<MLIcon iconTitle={ 'edit' } iconType={ 'edit' } iconFill='inherit' />} title='Primary' btnClass='primary' secondary='icon' />
          <MLButton icon={<MLIcon iconTitle={ 'arrow_left' } iconType={ 'arrow_left' } iconFill='inherit' />} title='Primary' btnClass='primary' secondary='icon' />
          <MLButton icon={<MLIcon iconTitle={ 'edit' } iconType={ 'edit' } iconFill='inherit' />} title='Primary' btnClass='primary' secondary='icon' />
          <pre>{`
<MLButton icon={<MLIcon iconTitle={ 'edit' } iconType={ 'edit' } iconFill='inherit' />} title='Primary' btnClass='primary' secondary='icon' />
<MLButton icon={<MLIcon iconTitle={ 'arrow_left' } iconType={ 'arrow_left' } iconFill='inherit' />} title='Primary' btnClass='primary' secondary='icon' />
<MLButton icon={<MLIcon iconTitle={ 'edit' } iconType={ 'edit' } iconFill='inherit' />} title='Primary' btnClass='primary' secondary='icon' />
          `}</pre>

        </div>
      </div>
    );
  }
}


export default _Entry;
