# E-Snippet Component

## Install

`npm install ml-esnippet`

# Usage

See examples folder for complete example. Make sure to `npm install`.

```javascript
import ESnippet from '../dist/esnippet';


<div className={ IndexCSS['mlreader-container'] }>
  <ESnippet
    vbid={ ebook.vbid }
    cfi={ ebook.cfi }
    start={ ebook.start }
    stop={ ebook.stop }
    cb={ this.optionalCallback } />
</div>
```

### Resources

* [CSSNext feature](http://cssnext.io/features)
